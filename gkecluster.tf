resource "google_container_cluster" "projterraform" {
  name               = "terraform-cluster2"
  project            = "projterraform"
  initial_node_count = 1
  network            = "myvpctest"
  location           = "us-central1"
  node_version       = "1.13.6-gke.0"
  min_master_version = "1.13.6-gke.0" 
}